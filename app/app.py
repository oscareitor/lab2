from flask import Flask

app = Flask(__name__)

@app.route("/")
def index():
    return "Index!"

@app.route("/route1")
def route1():
    return "route1!"

@app.route("/route2")
def route2():
    return "route2"

@app.route("/hello")
def hello():
    return "Hello World! Oscar Garcia"


@app.route("/members")
def members():
    return "Members"


def main():
    # listening in port 5000
    app.run(threaded=True, debug=True, host='0.0.0.0')

if __name__ == '__main__':
    main()
