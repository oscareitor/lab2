
from __future__ import print_function
import argparse
import logging
import sys
import boto3
import botocore

log = logging.getLogger('cfn_stack')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("stack_name", help="The CFN stack to update")
    parser.add_argument("template", help="The CFN Template to update the stack")
    args = parser.parse_args()

    log_level = logging.DEBUG
    logging.basicConfig(stream=sys.stdout, level=log_level)

    client = boto3.client('cloudformation')

    list_of_stacks = client.list_stacks()

    create = True
    for stack in list_of_stacks['StackSummaries']:
        if args.stack_name in stack['StackId'] and stack['StackStatus'] != 'DELETE_COMPLETE':
            create = False

    log.info(create)
    if create:
        client.create_stack(
            StackName=args.stack_name,
            TemplateBody=open(args.template, 'r').read(),
            Capabilities=[
                'CAPABILITY_NAMED_IAM'
            ],
        )
    else:
        try:
            client.update_stack(
                StackName=args.stack_name,
                TemplateBody=open(args.template, 'r').read(),
                Capabilities=[
                    'CAPABILITY_NAMED_IAM'
                ],
            )
        except botocore.exceptions.ClientError as ex:
            log.debug(ex)

if __name__ == "__main__":
    main()
